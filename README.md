# Test project for BE



## Instalation
```shell
$ yarn install
```

## Run
Dependency:
- MongoDB, url can be changed in `config.ts`

```shell
$ yarn serve
```

can access server on `http://localhost:4000/api`

## Development
Can start `mongo` from docker container by running
```
$ docker-compose up -d
```