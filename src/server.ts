import { debug } from "debug";
import config from "./../config";
import app from "./app";

debug.enable("*");

const log = debug("app");

const port = process.env.PORT || config.PORT;

app.listen(port, () => {
  log("Server listening on port:", port);
});
