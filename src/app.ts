import bodyParser from "body-parser";
import cors from "cors";
import express from "express";
import userRoute from "./users/user.controller";

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// api routes
app.use("/api/users", userRoute);

export default app;
