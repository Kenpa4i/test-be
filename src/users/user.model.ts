import mongoose from "mongoose";
import { IUserModel } from "./types";
const Schema = mongoose.Schema;

type UserDocument = mongoose.Document & IUserModel;

const userSchema = new Schema({
    createdDate: { type: Date, default: Date.now },
    updatedDate: { type: Date, default: Date.now },
    birthDate: { type: Date },
    email: { type: String, required: true, unique: true, trim: true },
    name: { type: String },
});

userSchema.set("toJSON", { virtuals: true });

export default mongoose.model<UserDocument>("User", userSchema);
