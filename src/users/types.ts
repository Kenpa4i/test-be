export interface IUserModel extends IUserData{
  createdDate: string;
  updatedDate: string;
}
export interface IUserData {
  birthDate: string;
  email: string;
  name: string;
}