import { Router } from "express";
import { RouteEndPoint } from "./../_helpers/types";
import { debug } from "debug";
import userService from "./user.service";

const router = Router();

const log = debug("User API | ");

const isDateStringValid = (date: string) => {
  return Date.parse(date) !== NaN;
}

const isDateInFuture = (date: string) => 
  !isDateStringValid(date) || Date.parse(date) >= Date.now();

const emailTakenMessage = (email: string) => `Email "${email}" is already taken`;

const isStringValidEmail = (email = '') => /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);

const create: RouteEndPoint = async (req, res, next) => {
  log(req.method, req.url);

  const email = req.body.email;

  if(!isStringValidEmail(email)){
    return res.status(400).send('email field not valid');
  }
  if(await userService.getByEmail(email)) {
    return res.status(400).send(emailTakenMessage(email));
  }
  const birthDate = req.body.birthDate;
  if (!!birthDate && isDateInFuture(birthDate)){
    return res.status(400).send('Date of birth should be in past');
  };

  userService.create(req.body)
    .then(data => res.json(data))
    .catch(err => next(err));
};

const getAll: RouteEndPoint = (req, res, next) => {
  log(req.method, req.url);
  userService.getAll()
    .then(data => res.json(data))
    .catch(next);
};

const getById: RouteEndPoint = (req, res, next) => {
  log(req.method, req.url);
  userService.getById(req.params.id)
    .then((user) => user ? res.json(user) : res.sendStatus(404))
    .catch(next);
};

const update: RouteEndPoint = async (req, res, next) => {
  log(req.method, req.url);

  const user = await userService.getById(req.params.id)
  if (!user){
    return res.sendStatus(404);
  }
  const email = req.body.email;

  if(!isStringValidEmail(email)){
    return res.status(400).send('email field not valid');
  }

  if (!!email && user.email !== email && await userService.getByEmail(email)) {
    return res.status(400).send(emailTakenMessage(email));
  }
  const birthDate = req.body.birthDate;
  if (!!birthDate && user.birthDate !== birthDate && isDateInFuture(birthDate)){
    return res.status(400).send('Date of birth should be in past');
  };

  userService.update(req.params.id, req.body)
    .then(data => res.json(data))
    .catch(next);
};

const remove: RouteEndPoint = (req, res, next) => {
  log(req.method, req.url);
  userService.remove(req.params.id)
    .then(() => res.json({}))
    .catch(next);
};

// routes
router.post("/create", create);
router.get("/", getAll);
router.get("/:id", getById);
router.put("/:id", update);
router.delete("/:id", remove);

export default router;
