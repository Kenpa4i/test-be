import db from "./../_helpers/db";
import {IUserData} from "./types";

const User = db.User;

async function getAll() {
  return await User.find();
}

const getById = async (id: string) =>
  await User.findById(id);
  
const getByEmail = async (email: string) => await User.findOne({email}); 


const create = async (userParam: IUserData) => {
    const user = new User(userParam);

    return await user.save();
};

const update = async (id: string, updatedData: Partial<IUserData>) => {
    const user = await User.findById(id);
    if (!user) { throw new Error("User not found"); }

    Object.assign(user, {
      ...updatedData,
      updatedDate: new Date(),
    });

    return await user.save();
};

const remove = async (id: string) => {
  await User.findByIdAndRemove(id);
};

export default {
  create,
  getAll,
  getById,
  getByEmail,
  update,
  remove,
};
