import { NextFunction, Request, Response } from "express";

export type RouteEndPoint = (req: Request, res: Response, next: NextFunction) => void;
