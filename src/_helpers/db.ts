import { connect } from "mongoose";
import config from "./../../config";
connect(process.env.MONGODB_URI || config.DB_URI, {
  dbName: "admin",
  useCreateIndex: true,
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
})
.then(() => console.log("DB server connected"))
.catch(e => console.log("DB error: ", e));

import User from "./../users/user.model";

export default {
  User,
};
